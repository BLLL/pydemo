
# is esptool installed on my system?
which esptool.py

# if it's not install it

https://pypi.org/project/esptool/

# get the firmware for your ESP8266
wget "https://micropython.org/resources/firmware/esp8266-20210902-v1.17.bin" .

# erease it!
sudo esptool.py --port /dev/ttyUSB0 erase_flash

# flash it!
sudo esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash --flash_size=detect 0 esp8266-20210902-v1.17.bin

# connect to it ( if you don't have it apt-get install picocom )
sudo picocom /dev/ttyUSB0 -b115200
