
# Test number 1 turn on/off the led
# Connect to the esp8266 via console o via webrpl

import machine
internalLed = machine.Pin(2, machine.Pin.OUT)
internalLed.on()
internalLed.off()

# Before experiment more
# If you want to experiment more.... here the Esp8266 pinout
https://micronote.tech/img/nodemcu_pinmap.jpg

# look at this page there is a simple program to turn on/off an external led
# pay attention to the fact that the pin marked as d1 on the board corrispond to pin number 5 by the pinout scheme
https://micronote.tech/2020/01/Basic-GPIO-Input-and-Output-with-a-NodeMCU-and-MicroPython/

# how do I get the file list inside the board
import os
arr = os.listdir()
print(arr)

# how do I put/get files from the board:
1- webrepl  trought the nice web interface
2- rshell   https://github.com/dhylands/rshell
3- mpfshell https://github.com/wendlers/mpfshell
